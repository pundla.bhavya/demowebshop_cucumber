package com.stepDef;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features="src/test/resources/Feature/demoshop.feature", glue = "com.stepDef")

public class TestRunnerTestNG extends AbstractTestNGCucumberTests{

}
